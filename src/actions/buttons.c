#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/freeglut.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

#include "../../include/aide.h"
#include "../../include/chaine.h"
#include "../../include/mariage.h"
#include "../../include/createIndividual.h"
#include "../../include/childLink.h"
#include "../../include/weddLink.h"

#include "../../include/My_GL.h"
#include "../../include/arbre.h"
#include "../../include/diagramme.h"
#include "../../include/statistique.h"
#include "../../include/texture.h"
#include "../../include/carto.h"
#include "../../include/ville.h"
#include "../../include/interface.h"
#include "../../include/buttons.h"
#include "../../include/ia.h"

#define RAYON 8

void btnScreenshot(int lF, int hF){
	savePNGPicture("../ressources/output.bmp", lF, hF);
	printf("picture saved output.bmp saved in Ressources folder !\n");
	//Appel systeme pour ouvrir l'image
	system("eog ../ressources/output.bmp &");
}

void btnLocaliser(individual *id, OBJ3D* pinedCity, char* LastCity){
	if(id != NULL){
		printf("🌍 Localising the individual... please wait ... 🌍\n");
		//Verification de l'allocation mémoire
		if(pinedCity != NULL) {
			//Verification afin de ne pas traiter deux fois de suite une même ville
			if( strcmp( id->infos.birth.location, LastCity) || pinedCity->x== 0){
				*pinedCity = placeVille(id->infos.birth.location, RAYON-2);
			}
			else{
				printf("City of %s already pinned !\n",LastCity);
			}
			strcpy(LastCity, id->infos.birth.location);
		}
	}
}

void btnProcessIA(individual* ptrListe,wedding* ptrMariage){
	find_lieu_naissance(ptrListe);
	find_date_naissance(ptrListe);
	find_lieu_mort(ptrListe);
	find_lieu_mariage(ptrMariage);
	find_date_mariage(ptrMariage);
	find_date_mort(ptrListe);
}

void btnSaveCSV(individual *ptrListe, wedding *ptrMariage){
	printf("Sauvegarde du fichier individus... : ../ressources/IndivSauvegarde.csv\n");
	ecris_indiv(ptrListe , "../ressources/IndivSauvegarde.csv");
	
	printf("Sauvegarde du fichier mariages... : ../ressources/mariagesSauvegarde.csv\n");
	ecris_fichier_mariage("../ressources/mariagesSauvegarde.csv", ptrMariage);
	
	printf("Fichiers sauvegardés\n");
}

void btnShadow(int *shadow, int menu, int areaSelected){
	if(menu){
		if (*shadow == 1){
			*shadow = 0;
			glDisable(GL_FOG);
		}
		else{
			*shadow = 1;
		}
	}
}

void btnGoogleMaps(individual *id){
	if(id !=NULL && strcmp(id->infos.birth.location, "") != 0){
		char nav_loc[200];
		sprintf(nav_loc, "x-www-browser https://www.google.com/maps/place/%s", id->infos.birth.location);
		system(nav_loc);
	}
	else{
		char nav_loc[200];
		sprintf(nav_loc, "x-www-browser https://www.google.com/maps/");
		system(nav_loc);
	}
}


void btnpanClose(int * panelActive){
	*panelActive = -1;
}

individual* btnSearch(char *search, individual *ptrTete, int genmax){
	char c = search[0]; int index = 0; int hasSpace = 0;
	int indexSpace = 0;
	individual *idd = NULL;
	
	printf(" research Indiv = '%s'\n", search);
    printf("search button toggled!\n");
    
	
	while( c != '\0'){
		if(c == ' '){ //si egal au caractere ASCII espace
			hasSpace++;
			printf("espace détécté à emplacement %d\n",index);
			indexSpace = index;
		}
		index++;
		c = search[index];
	}
	
	if(hasSpace == 1){
		printf("La chaine possède un espace\n");
		char* nom = search+indexSpace+1;
		char prenom[50];
		strcpy(prenom, search);
		prenom[indexSpace] = '\0';
		
		printf("1er mot : '%s'\n",nom);
		printf("2eme mot : '%s'\n",prenom);
		
		//On recherche pour chaque gen
		int gen = 1;
		while(gen <= genmax){
			printf("gen = %d\n",gen);
			idd = rechercheIndiv( nom, prenom, gen, ptrTete);
			if(idd !=NULL){
				gen = genmax+1;
			}
			else{
				gen++;
			}
		}
		
		if(idd == NULL){ //Recherche infructueuse : on essaie d'inverser les champs
			gen = 1;
			while(gen <= genmax){
				printf("gen = %d\n",gen);
				idd = rechercheIndiv( prenom, nom, gen, ptrTete);
				if(idd !=NULL){
					gen = genmax+1;
				}
				else{
					gen++;
				}
			}
		}
		
		if(idd != NULL){
			printf("id n'est plus NULL\n");
			return idd;
		}
		else{
			printf("No individual found ! \n");
		}
	}
	else if(hasSpace == 0){
		printf("pas d'espace !\n");
		//Pas d'espace, on cherche un nom unique
		individual *ptr= ptrTete ;
		
		while(ptr != NULL && idd==NULL){
			if(strcasecmp(search, ptr->infos.name) == 0)
				idd = ptr;
			else if(strcasecmp(search, ptr->infos.surname) == 0)
				idd = ptr;
			ptr = ptr->next;
		}
		if(idd !=NULL && strcmp(idd->infos.name,"")){
			printf("id n'est plus NULL\n");
			printf("individu identifié : %s %s\n",idd->infos.name, idd->infos.surname);
			return idd;
		}
		else{
			printf("No individual found ! \n");
		}
	}
	return NULL;

}

void btnLink(void){
    printf("link button toggled!\n");
}

void btnInsert(individual **id, int *panelActive, char tabInfos[NB_INFOS][150], char baseInfos[NB_INFOS][150]){
	*panelActive = 1;
	printf("panel Active set to 1\n");
	*id = NULL;
    for(int i=0; i<NB_INFOS; i++){
		strcpy(tabInfos[i],baseInfos[i]);
		if(i != 2)
		strcat(tabInfos[i], " Click to edit");
	}
}

void btnModifier(int *panelActive, char tabInfos[NB_INFOS][150], char baseInfos[NB_INFOS][150]){
	*panelActive = 2;
	printf("panel Active set to 2\n");
	
	for(int i=0; i<NB_INFOS; i++){
		strcpy(tabInfos[i],baseInfos[i]);
		if(i != 2)
		strcat(tabInfos[i], " Click to edit");
	}
    
}

void btnDelete(individual indiv_to_insert, int *panelActive, char tabInfos[NB_INFOS][150], char baseInfos[NB_INFOS][150]){
	*panelActive = 3;
	printf("panel Active set to 3\n");
	
	for(int i=0; i<NB_INFOS; i++){
		strcpy(tabInfos[i],baseInfos[i]);
		strcat(tabInfos[i], "Unknown");
	}
}

void btnApplyDelete(individual *id, Indiv3D *indiv_chosen, individual **ptrListe, Indiv3D** indiv_coords,  int* tabGen, int *panelActive){
	if(id != NULL){
		*indiv_chosen = *getIndiv3DfromIndivListe(id->next, indiv_coords, tabGen, id->infos.num_gen - 1);
		supprIndiv(id, ptrListe);
		id = NULL;
	}
	*panelActive = 0;
}

bool isValid(individual indiv, int genmax, individual *ptrListe){
	if( strcmp(indiv.infos.name,"") 
	&& strcmp(indiv.infos.surname,"") 
	&& (indiv.infos.gender=='M' || indiv.infos.gender=='F')
	&& (indiv.infos.num_gen> 0 && indiv.infos.num_gen <= genmax)
	){
		//On verifie que l'individu n'existe pas encore
		if( rechercheIndiv(indiv.infos.surname, indiv.infos.name, indiv.infos.num_gen, ptrListe) == NULL ) {
			return true;
		}
	}
	
	return false;
}

void btnApplyInsert(individual *indiv_to_insert, int *panelActive, individual **ptrListe, int genmax){
	if(indiv_to_insert != NULL && isValid(*indiv_to_insert, genmax, *ptrListe)){
		*panelActive = 0; individual *mere = NULL; individual *pere = NULL;
		if(indiv_to_insert -> father != NULL){
			pere = rechercheIndiv(indiv_to_insert->father->infos.surname, indiv_to_insert->father->infos.name, indiv_to_insert->infos.num_gen+1, *ptrListe);
		}
		if(indiv_to_insert -> mother != NULL){
			mere = rechercheIndiv(indiv_to_insert->mother->infos.surname, indiv_to_insert->mother->infos.name, indiv_to_insert->infos.num_gen+1, *ptrListe);
		}
		newIndividual(indiv_to_insert->infos, ptrListe, pere, mere);
	}
}

void btnApplyEdit(int *panelActive){
	*panelActive = 0;
}


